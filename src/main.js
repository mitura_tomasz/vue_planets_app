import Vue from 'vue'
import App from './App.vue'
import VueResource from 'vue-resource';

import store from './stores/store';

import PlanetsList from './components/PlanetsList.vue';
import PlanetDescription from './components/PlanetDescription.vue';

Vue.component('app-planets-list', PlanetsList );
Vue.component('app-planet-description', PlanetDescription );

Vue.config.productionTip = false

Vue.use(VueResource);

new Vue({
  render: h => h(App),
  store
}).$mount('#app')
