import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    favoritePlanets: [],
    activePlanet: null
  },
  mutations: { // syncronous
    setActivePlanet(state, planet) {
      state.activePlanet = planet
    },
    addActivePlanetToFavorites(state) {
      let isActivePlanetInFavorites = state.favoritePlanets.some(x => x == state.activePlanet)

      if (!isActivePlanetInFavorites) {
        state.favoritePlanets.push( state.activePlanet )
      }
    },
    removeActivePlanetFromFavorites(state) {
      let planets = [];

      state.favoritePlanets.map(x => {
        if (x != state.activePlanet) { planets.push(x) }
      })

      state.favoritePlanets = planets
    }
  },
  actions: { // asyncronous
    setActivePlanet(state, planet) {
      state.commit('setActivePlanet', planet)
    },
    addActivePlanetToFavorites(state) {
      state.commit('addActivePlanetToFavorites')
    },
    removeActivePlanetFromFavorites(state) {
      state.commit('removeActivePlanetFromFavorites')
    }
  },
  getters: {
    activePlanet(state) {
      return state.activePlanet
    },
    favoritePlanets(state) {
      return state.favoritePlanets
    }
  }
})

export default store